//Copyright (C) 2013  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.ide;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.lang.reflect.Method;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.BasicHttpEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.nuclos.api.ide.SourceItemFacadeRemote;
import org.nuclos.api.ide.valueobject.NuclosApi;
import org.nuclos.api.ide.valueobject.SourceItem;
import org.nuclos.api.ide.valueobject.SourceType;
import org.springframework.remoting.support.RemoteInvocation;
import org.springframework.remoting.support.RemoteInvocationResult;

public class SourceItemTest {

	public static void main(String[] args) throws ClientProtocolException, IOException, URISyntaxException, NoSuchMethodException, SecurityException {
		testConnection(getBaseURI());
		testGetDownloadURL(getBaseURI());
		testGetTree(getBaseURI());
	}

	private static URI getBaseURI() throws URISyntaxException {
		return new URI(Constants.getBaseURL() + "/remoting/SourceItemService");
	}
	
	private static void testConnection(URI uri) throws NoSuchMethodException, SecurityException, IOException {
		// see https://hc.apache.org/httpcomponents-client-ga/quickstart.html
		final DefaultHttpClient httpclient = new DefaultHttpClient();
		final HttpPost httpPost = new HttpPost(uri);
		
		final Method getNuclosApi = SourceItemFacadeRemote.class.getDeclaredMethod("getNuclosApi");
		final HttpEntity httpEntitiy = invokeEntity(getNuclosApi);
		httpPost.setEntity(httpEntitiy);

		// The underlying HTTP connection is still held by the response object 
		// to allow the response content to be streamed directly from the network socket. 
		// In order to ensure correct deallocation of system resources 
		// the user MUST either fully consume the response content  or abort request 
		// execution by calling HttpGet#releaseConnection().

		try {
			HttpResponse response1 = httpclient.execute(httpPost);
			System.out.println("status: " + response1.getStatusLine());
			for (Header h : response1.getAllHeaders()) {
				System.out.println("header: " + h);
			}
			HttpEntity entity1 = response1.getEntity();
			long len = entity1.getContentLength();
			System.out.println("length: " + len);
			System.out.println("type: " + entity1.getContentType());
			System.out.println("content:");
			
			final NuclosApi api = extractResult(entity1.getContent());
			System.out.println("Nuclos API: " + api.getNuclosVersion() + " jars: " + api.getNuclosApiJars());
			
			// do something useful with the response body
			// and ensure it is fully consumed
			EntityUtils.consume(entity1);
		} catch (Exception e2) {
			System.out.println(e2.toString());
			e2.printStackTrace();
		} finally {
			httpPost.releaseConnection();
		}
	}
	
	private static void testGetDownloadURL(URI uri) throws URISyntaxException, ClientProtocolException, IOException, NoSuchMethodException, SecurityException {
		// see https://hc.apache.org/httpcomponents-client-ga/quickstart.html
		final DefaultHttpClient httpclient = new DefaultHttpClient();
		final HttpPost httpPost = new HttpPost(uri);
		
		final Method getDownloadURL = SourceItemFacadeRemote.class.getDeclaredMethod("getDownloadURL", 
				new Class<?>[] { String.class, boolean.class });
		final HttpEntity httpEntitiy = invokeEntity(getDownloadURL, "org.nuclos.businessentity.nuclosuser", false);
		httpPost.setEntity(httpEntitiy);

		// The underlying HTTP connection is still held by the response object 
		// to allow the response content to be streamed directly from the network socket. 
		// In order to ensure correct deallocation of system resources 
		// the user MUST either fully consume the response content  or abort request 
		// execution by calling HttpGet#releaseConnection().

		try {
			HttpResponse response1 = httpclient.execute(httpPost);
			System.out.println("status: " + response1.getStatusLine());
			for (Header h : response1.getAllHeaders()) {
				System.out.println("header: " + h);
			}
			HttpEntity entity1 = response1.getEntity();
			long len = entity1.getContentLength();
			System.out.println("length: " + len);
			System.out.println("type: " + entity1.getContentType());
			System.out.println("content:");
			
			final URL url = extractResult(entity1.getContent());
			System.out.println("URL: " + url);
			
			// do something useful with the response body
			// and ensure it is fully consumed
			EntityUtils.consume(entity1);
		} catch (Exception e2) {
			System.out.println(e2.toString());
			e2.printStackTrace();
		} finally {
			httpPost.releaseConnection();
		}
	}
	
	private static void testGetTree(URI uri) throws URISyntaxException, ClientProtocolException, IOException, NoSuchMethodException, SecurityException {
		// see https://hc.apache.org/httpcomponents-client-ga/quickstart.html
		final DefaultHttpClient httpclient = new DefaultHttpClient();
		final HttpPost httpPost = new HttpPost(uri);
		
		final Method getTree = SourceItemFacadeRemote.class.getDeclaredMethod("getTree", 
				new Class<?>[] { SourceType.class, String.class, boolean.class });
		final HttpEntity httpEntitiy = invokeEntity(getTree, null, null, false);
		httpPost.setEntity(httpEntitiy);

		// The underlying HTTP connection is still held by the response object 
		// to allow the response content to be streamed directly from the network socket. 
		// In order to ensure correct deallocation of system resources 
		// the user MUST either fully consume the response content  or abort request 
		// execution by calling HttpGet#releaseConnection().

		try {
			HttpResponse response1 = httpclient.execute(httpPost);
			System.out.println("status: " + response1.getStatusLine());
			for (Header h : response1.getAllHeaders()) {
				System.out.println("header: " + h);
			}
			HttpEntity entity1 = response1.getEntity();
			long len = entity1.getContentLength();
			System.out.println("length: " + len);
			System.out.println("type: " + entity1.getContentType());
			System.out.println("content:");
			
			final SourceItem si = extractResult(entity1.getContent());
			System.out.println("SourceItem: " + si.getQualifiedName() + "type: " + si.getType() + " children" + si.getChildren());
			
			// do something useful with the response body
			// and ensure it is fully consumed
			EntityUtils.consume(entity1);
		} catch (Exception e2) {
			System.out.println(e2.toString());
			e2.printStackTrace();
		} finally {
			httpPost.releaseConnection();
		}
	}
	
	private static <T> T extractResult(InputStream rawIn) throws IOException, ClassNotFoundException {
		final ObjectInputStream in = new ObjectInputStream(rawIn); 
		final RemoteInvocationResult result;
		try {
			result = (RemoteInvocationResult) in.readObject();
			if (in.read() >= 0) {
				throw new IllegalStateException();
			}
		} finally {
			in.close();
		}
		System.out.println("java object: " + result);
		if (result != null) {
			System.out.println("java object class: " + result.getClass().getName());
		}
		final Object value = result.getValue();
		final Throwable ex = result.getException();
		System.out.println("value: " + value);
		if (value != null) {
			System.out.println("value class: " + value.getClass().getName());
		}
		System.out.println("exception: " + ex);
		if (ex != null) {
			System.out.println("exception class: " + ex.getClass().getName());
		}
		return (T) result.getValue();
	}
	
	private static HttpEntity invokeEntity(Method method, Object... arguments) throws IOException {
		final BasicHttpEntity httpEntitiy = new BasicHttpEntity();
		final RemoteInvocation ri = new RemoteInvocation();
		ri.setParameterTypes(method.getParameterTypes());
		ri.setMethodName(method.getName());
		ri.setArguments(arguments);
		
		final PipedOutputStream pout = new PipedOutputStream();
		final PipedInputStream pin = new PipedInputStream(pout);
		httpEntitiy.setContent(pin);
		final ObjectOutputStream out = new ObjectOutputStream(pout);
		
		final Thread thread = new Thread() {
			@Override
			public void run() {
				try {
					out.writeObject(ri);
					out.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		};
		thread.start();
		
		return httpEntitiy;
	}

}
