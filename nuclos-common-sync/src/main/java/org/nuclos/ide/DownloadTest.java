//Copyright (C) 2013  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.ide;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

public class DownloadTest {

	public static void main(String[] args) throws ClientProtocolException, IOException, URISyntaxException {
		System.out.println("Hello World!");

		URI nuc = getClassURI("org.nuclos.businessentity.nuclosuser");
		URI nus = getSourceURI("org.nuclos.businessentity.nuclosuser");
		test(nuc);
		test(nus);
	}

	private static URI getBaseURI() throws URISyntaxException {
		return new URI(Constants.getBaseURL() + "/sidownload");
	}
	
	private static URI getClassURI(String qname) throws URISyntaxException {
		return new URI(getBaseURI().toString() + "/" + qname.replace('.', '/') + ".class");
	}

	private static URI getSourceURI(String qname) throws URISyntaxException {
		return new URI(getBaseURI().toString() + "/" + qname.replace('.', '/') + ".java");
	}

	private static void test(URI uri) throws URISyntaxException, ClientProtocolException, IOException {
		// see https://hc.apache.org/httpcomponents-client-ga/quickstart.html
		final DefaultHttpClient httpclient = new DefaultHttpClient();
		final HttpGet httpGet = new HttpGet(uri);

		// The underlying HTTP connection is still held by the response object 
		// to allow the response content to be streamed directly from the network socket. 
		// In order to ensure correct deallocation of system resources 
		// the user MUST either fully consume the response content  or abort request 
		// execution by calling HttpGet#releaseConnection().

		try {
			HttpResponse response1 = httpclient.execute(httpGet);
			System.out.println("status: " + response1.getStatusLine());
			for (Header h : response1.getAllHeaders()) {
				System.out.println("header: " + h);
			}
			HttpEntity entity1 = response1.getEntity();
			long len = entity1.getContentLength();
			System.out.println("length: " + len);
			System.out.println("type: " + entity1.getContentType());
			System.out.println("content:");
			final byte[] buffer = new byte[4096];
			final InputStream in = entity1.getContent();
			int i;
			try {
				while (len > 0 && (i = in.read(buffer)) >= 0) {
					len -= i;
					if (i == 0) {
						Thread.yield();
					} else if (i < 0) {
						break;
					} else {
						System.out.print(new String(buffer, 0, i));
					}
				}
			} catch (Exception e1) {
				System.out.println(e1.toString());
				e1.printStackTrace();
			} finally {
				in.close();
			}
			System.out.println();
			// do something useful with the response body
			// and ensure it is fully consumed
			EntityUtils.consume(entity1);
		} catch (Exception e2) {
			System.out.println(e2.toString());
			e2.printStackTrace();
		} finally {
			httpGet.releaseConnection();
		}
	}

}
