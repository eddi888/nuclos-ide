package org.nuclos.ide;

public class Constants {
	
	private Constants() {
		// Never invoked.
	}
	
	public static String getBaseURL() {
		return "http://localhost:8090/nuclos-war";
	}

}
