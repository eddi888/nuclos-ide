//Copyright (C) 2014  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.ide;

import org.eclipse.core.resources.IResourceChangeEvent;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.nuclos.ide.listener.ServerCleanupListener;
import org.osgi.framework.BundleContext;

/**
 * The activator class controls the plug-in life cycle
 */
public class Activator extends AbstractUIPlugin {

	// The plug-in ID
	public static final String PLUGIN_ID = "org.nuclos.ide"; //$NON-NLS-1$

	// The shared instance
	private static Activator plugin;
	
	//
	
	private ServerCleanupListener cleanupListener;
	
	/**
	 * The constructor
	 */
	public Activator() {
	}

	public void start(BundleContext context) throws Exception {
		super.start(context);
		
		plugin = this;
		cleanupListener = new ServerCleanupListener();
		final IWorkspace ws = ResourcesPlugin.getWorkspace();
		ws.addResourceChangeListener(cleanupListener, IResourceChangeEvent.PRE_DELETE); 
	}

	public void stop(BundleContext context) throws Exception {
		final IWorkspace ws = ResourcesPlugin.getWorkspace();
		ws.removeResourceChangeListener(cleanupListener);
		
		plugin = null;
		cleanupListener = null;
		
		super.stop(context);
	}

	/**
	 * Returns the shared instance
	 *
	 * @return the shared instance
	 */
	public static Activator getDefault() {
		return plugin;
	}

}
