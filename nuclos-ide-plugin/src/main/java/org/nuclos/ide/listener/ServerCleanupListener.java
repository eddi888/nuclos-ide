//Copyright (C) 2014  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.ide.listener;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResourceChangeEvent;
import org.eclipse.core.resources.IResourceChangeListener;
import org.eclipse.core.resources.IResourceDelta;
import org.eclipse.core.runtime.CoreException;
import org.nuclos.ide.server.ServerDebug;
import org.nuclos.ide.server.ServerNature;

public class ServerCleanupListener implements IResourceChangeListener {

	public ServerCleanupListener() {
	}
	
	@Override
	public void resourceChanged(IResourceChangeEvent event) {
		tryToRemoveDebugConfiguration(event);
	}
	
	private void tryToRemoveDebugConfiguration(IResourceChangeEvent event) {
		try {
			IResourceDelta serverProject = getServerProjectDelta(event.getDelta());
			if (serverProject != null 
					&& serverProject.getKind() == IResourceDelta.REMOVED) {
				ServerDebug.deleteConfiguration((IProject) serverProject.getResource());
			}
		} catch (CoreException ex) {
			// ignore
			ex.printStackTrace();
		}
	}
	
	private IResourceDelta getServerProjectDelta(IResourceDelta delta) throws CoreException {
		// TODO hasNature throws Resource does not exist Exception... Why? Event is PRE_DELETE?!
		// 
		// Perhaps the IProject is already closed and hasNature doesn't work any more? (tp)
		if (true) return null;
		
		if (delta.getResource() instanceof IProject
					&& ((IProject) delta.getResource()).hasNature(ServerNature.NATURE_ID)) {
			return delta;
		}
		if (delta.getAffectedChildren() != null) {
			for (IResourceDelta child : delta.getAffectedChildren()) {
				IResourceDelta found = getServerProjectDelta(child);
				if (found != null) {
					return found;
				}
			}
		}
		return null;
	}

}
