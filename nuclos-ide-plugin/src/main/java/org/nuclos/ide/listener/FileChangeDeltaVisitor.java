//Copyright (C) 2014  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.ide.listener;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceDelta;
import org.eclipse.core.resources.IResourceDeltaVisitor;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.nuclos.ide.NuclosFatalIdeException;
import org.nuclos.ide.server.ServerNature;
import org.nuclos.ide.singleton.SourceItemCache;

public class FileChangeDeltaVisitor implements IResourceDeltaVisitor {
	
	private final SourceItemCache sic;

	private final IProject project;
	
	public FileChangeDeltaVisitor(IProject project) throws CoreException {
		if (!project.hasNature(ServerNature.NATURE_ID)) {
			throw new NuclosFatalIdeException("Not a Nuclos Server Rules Project");
		}
		this.project = project;
		this.sic = SourceItemCache.getInstance(project);
	}

	@Override
	public boolean visit(IResourceDelta delta) throws CoreException {
		final IResource resource = delta.getResource();
		final IProject p = resource.getProject();
		if (p == null) {
			// WorkspaceRoot & Co.
			return true;
		}

		boolean result;
		if (p.hasNature(ServerNature.NATURE_ID)) {
			if (!p.equals(project)) {
				return false;
			}
			if (resource.equals(p)) {
				return true;
			}
			final IFolder ruleDir = p.getFolder(ServerNature.SERVERRULE);
			final IPath rulePath = ruleDir.getLocation();
			final IPath filePath = resource.getLocation();
			if (!rulePath.isPrefixOf(filePath)) {
				return false;
			}
			final int type = resource.getType();
			final int kind = delta.getKind();
			switch (type) {
			case IResource.FILE:
				if (!resource.isDerived(IResource.CHECK_ANCESTORS)
						&& !resource.isLinked(IResource.CHECK_ANCESTORS)
						&& !resource.isPhantom()) {
					final IFile file = (IFile) resource;
					final IPath qpath = file.getLocation().removeFileExtension().removeFirstSegments(
							rulePath.segmentCount()).setDevice(null);
					final String qname = qpath.toPortableString().replaceAll("/", ".");
					if (kind == IResourceDelta.REMOVED || kind == IResourceDelta.REMOVED_PHANTOM) {
						sic.remove(qname);
					} else {
						sic.dirty(qname);
					}
				}
				result = false;
				break;
			case IResource.FOLDER:
				result = true;
				break;
			default:
				result = false;
			}
		} else {
			result = false;
		}
		return result;
	}

}
