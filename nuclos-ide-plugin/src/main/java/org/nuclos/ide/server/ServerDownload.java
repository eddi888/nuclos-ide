//Copyright (C) 2014  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.ide.server;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.WorkspaceJob;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.jobs.ISchedulingRule;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.core.runtime.jobs.MultiRule;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.internal.core.JavaModelManager;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.nuclos.ide.Utils;
import org.nuclos.ide.singleton.Errors;

/**
 * Handler/listener triggering download from Nuclos server.
 * 
 * @author Maik Stuecker, Thomas Pasch
 */
public class ServerDownload extends AbstractHandler implements MouseListener {
	
	private IJavaProject javaProject;
	
	public ServerDownload() {
	}

	public ServerDownload(IJavaProject project) {
		this.javaProject = project;
	}
	
	private IJavaProject getJavaProject() {
		if (javaProject == null) {
			javaProject = Utils.getSelectedProject();
		}
		return javaProject;
	}

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		final IJavaProject javaProject = getJavaProject();
		execute(javaProject);
		return null;
	}
	
	public static void execute(final IJavaProject javaProject) {
		final IProject project = javaProject.getProject();
		final Errors errors = Errors.getInstance(project);
		errors.clear();
		final ServerTransfer transfer = new ServerTransfer(javaProject);
		final IWorkspace ws = javaProject.getProject().getWorkspace();
		
		// http://wiki.eclipse.org/FAQ_How_do_I_prevent_builds_between_multiple_changes_to_the_workspace%3F
		// http://help.eclipse.org/juno/index.jsp?topic=%2Forg.eclipse.platform.doc.isv%2Freference%2Fapi%2Forg%2Feclipse%2Fcore%2Fresources%2FWorkspaceJob.html
		//
		final WorkspaceJob job = new WorkspaceJob("Nuclos Server Download") {
			
			@Override
			public IStatus runInWorkspace(IProgressMonitor monitor) throws CoreException {
				try {
					transfer.download(monitor);
					NuclosBuilder.justDownloaded(javaProject.getProject());
					return errors.getStatus();
				} finally {
					errors.showOnErrors();
				}
			}
		};
		// we need exclusive access to project resources (Files, Folders)
		// in addition this will modify external folders (=classpath from Java VM)
		final ISchedulingRule[] rules = new ISchedulingRule[2];
		rules[0] = javaProject.getProject();
		rules[1] = JavaModelManager.getExternalManager().getExternalFoldersProject();
		job.setRule(new MultiRule(rules));
		job.setUser(true);
		job.setPriority(Job.LONG);
		job.schedule(200);
	}

	@Override
	public void mouseDoubleClick(MouseEvent arg0) {
	}

	@Override
	public void mouseDown(MouseEvent arg0) {
	}

	@Override
	public void mouseUp(MouseEvent arg0) {
		final IJavaProject javaProject = getJavaProject();
		execute(javaProject);
	}

}
