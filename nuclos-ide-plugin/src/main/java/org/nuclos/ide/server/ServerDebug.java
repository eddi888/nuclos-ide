//Copyright (C) 2014  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.ide.server;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.debug.core.DebugPlugin;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchConfigurationType;
import org.eclipse.debug.core.ILaunchConfigurationWorkingCopy;
import org.eclipse.debug.core.ILaunchManager;
import org.eclipse.debug.ui.IDebugUIConstants;
import org.eclipse.jdt.launching.IJavaLaunchConfigurationConstants;

/**
 * Setup an server debug configuration.
 * 
 * @author Maik Stuecker, Thomas Pasch (javadoc)
 */
public class ServerDebug implements IJavaLaunchConfigurationConstants {

	public static void createConfiguration(IProject project) throws CoreException {
		ILaunchManager manager = DebugPlugin.getDefault().getLaunchManager();
		ILaunchConfigurationType type = manager.getLaunchConfigurationType(ID_REMOTE_JAVA_APPLICATION);

		String lcName = manager.generateLaunchConfigurationName(project.getName());
		ILaunchConfigurationWorkingCopy lcwc = type.newInstance(null, lcName);
		
		lcwc.setAttribute(ATTR_PROJECT_NAME, project.getName());
		lcwc.setAttribute(ATTR_VM_CONNECTOR, ID_SOCKET_ATTACH_VM_CONNECTOR);
		Map<String, String> connectArgs = new HashMap<String, String>();
		connectArgs.put("hostname", project.getPersistentProperty(ServerNature.PROPERTY_HOST));
		connectArgs.put("port", project.getPersistentProperty(ServerNature.PROPERTY_DEBUG_PORT));
		lcwc.setAttribute(ATTR_CONNECT_MAP, connectArgs);
		List<String> favorites = new ArrayList<String>();
		favorites.add(IDebugUIConstants.ID_DEBUG_LAUNCH_GROUP);
		lcwc.setAttribute(IDebugUIConstants.ATTR_FAVORITE_GROUPS, favorites);

		// for future...
//			DebugUITools...
//			ILaunchConfiguration lc = 
		lcwc.doSave();

		project.setPersistentProperty(ServerNature.PROPERTY_LAUNCHLISTENER_NAME, lcName);
	}
	
	public static void deleteConfiguration(IProject project) throws CoreException {
		ILaunchConfiguration lc = getLaunchConfiguration(project);
		if (lc != null) {
			lc.delete();
		}
	}
	
	public static void updateConfiguration(IProject project) throws CoreException {
		ILaunchConfiguration lc = getLaunchConfiguration(project);
		if (lc != null) {
			Map<String, String> connectArgs = lc.getAttribute(ATTR_CONNECT_MAP, new HashMap<String, String>());
			connectArgs.put("hostname", project.getPersistentProperty(ServerNature.PROPERTY_HOST));
			connectArgs.put("port", project.getPersistentProperty(ServerNature.PROPERTY_DEBUG_PORT));
			
			ILaunchConfigurationWorkingCopy lcwc = lc.getWorkingCopy();
			lcwc.setAttribute(ATTR_CONNECT_MAP, connectArgs);
			lcwc.doSave();
		}
	}
	
	private static ILaunchConfiguration getLaunchConfiguration(IProject project) throws CoreException {
		ILaunchManager manager = DebugPlugin.getDefault().getLaunchManager();
		ILaunchConfigurationType type = manager.getLaunchConfigurationType(ID_REMOTE_JAVA_APPLICATION);
		
		String lcName = project.getPersistentProperty(ServerNature.PROPERTY_LAUNCHLISTENER_NAME);
		if (lcName == null) {
			return null;
		}
		for (ILaunchConfiguration lc : manager.getLaunchConfigurations(type)) {
			if (lc != null && lcName.equals(lc.getName())) {
				return lc;
			}
		}
		return null;
	}
	
}
