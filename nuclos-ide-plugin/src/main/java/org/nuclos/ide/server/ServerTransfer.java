//Copyright (C) 2014  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.ide.server;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.atomic.AtomicReference;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceVisitor;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.internal.ui.util.ExceptionHandler;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Display;
import org.nuclos.api.ide.valueobject.INuclosApi;
import org.nuclos.api.ide.valueobject.ISourceItem;
import org.nuclos.api.ide.valueobject.ISourceVisitor;
import org.nuclos.api.ide.valueobject.SourceItem;
import org.nuclos.api.ide.valueobject.SourceType;
import org.nuclos.digest.MessageDigest;
import org.nuclos.ide.ClasspathEntryComparator;
import org.nuclos.ide.NuclosFatalIdeException;
import org.nuclos.ide.Utils;
import org.nuclos.ide.singleton.Errors;
import org.nuclos.ide.singleton.HttpSingleton;
import org.nuclos.ide.singleton.SourceItemCache;

/**
 * Central class for communicating with Nuclos server. Implement upload/download.
 * 
 * @author Maik Stuecker, Thomas Pasch
 */
public class ServerTransfer {
	
	private static final AtomicReference<IProject> RUNNING = new AtomicReference<IProject>();
	
	//
	
	private final SourceItemCache siCache;
	
	private final Errors errors;
	
	private final IClasspathEntry[] EMPTY_CPE_ARRAY = new IClasspathEntry[0];

	private final IJavaProject jProject;
	
	private int ticksPerItem;
	
	public ServerTransfer(IJavaProject jProject) {
		this.jProject = jProject;
		this.siCache = SourceItemCache.getInstance(jProject.getProject());
		this.errors = Errors.getInstance(jProject.getProject());
	}
	
	private void downloadSources(IProgressMonitor monitor) {
		try {
			/*
			getDirServerrule().delete(true, null);
			getDirBusinessobject().delete(true, null);
			getDirStatemodel().delete(true, null);
			getDirGeneration().delete(true, null);
			getDirReport().delete(true, null);
			getDirReportDs().delete(true, null);
			getDirImportStructureDefinition().delete(true, null);
			getDirPrintout().delete(true, null);
			getDirParameter().delete(true, null);
			 */
			Utils.createFolder(getDirServerrule());
			Utils.createFolder(getDirBusinessobject());
			Utils.createFolder(getDirStatemodel());
			Utils.createFolder(getDirUserrole());
			Utils.createFolder(getDirGeneration());
			Utils.createFolder(getDirReport());
			Utils.createFolder(getDirReportDs());
			Utils.createFolder(getDirImportStructureDefinition());
			Utils.createFolder(getDirPrintout());
			Utils.createFolder(getDirParameter());
			final SourceItemVisitor siv = new SourceItemVisitor(monitor);
			readTree(siv, monitor);
			
			final Set<String> downloaded = siv.getDownloadedItems();
			getDirServerrule().accept(new FileRemover(getDirServerrule(), downloaded));
			getDirBusinessobject().accept(new FileRemover(getDirBusinessobject(), downloaded));
			getDirStatemodel().accept(new FileRemover(getDirStatemodel(), downloaded));
			getDirGeneration().accept(new FileRemover(getDirGeneration(), downloaded));
			getDirReport().accept(new FileRemover(getDirReport(), downloaded));
			getDirReportDs().accept(new FileRemover(getDirReportDs(), downloaded));
			getDirImportStructureDefinition().accept(new FileRemover(getDirImportStructureDefinition(), downloaded));
			getDirPrintout().accept(new FileRemover(getDirPrintout(), downloaded));
			getDirParameter().accept(new FileRemover(getDirParameter(), downloaded));
		} catch (CoreException ex) {
			/*
			IStatus warning = new Status(IStatus.ERROR, Activator.PLUGIN_ID, 1, "Nuclos source download failed", ex);
			ErrorDialog.openError(null, "Nuclos IDE", null, warning);
			throw new NuclosFatalIdeException(ex);
			 */
			errors.addError("Nuclos source download failed: " + ex);
		}
	}
	
	public void upload(IProgressMonitor monitor) throws CoreException {
		if (RUNNING.compareAndSet(null, jProject.getProject())) {
			try {
				final int severity = jProject.getProject().findMaxProblemSeverity(IMarker.PROBLEM, true,
						IResource.DEPTH_INFINITE);
				if (severity < IMarker.SEVERITY_ERROR) {
					checkMonitor(monitor);
					monitor.beginTask("uploading...", 200);
					uploadSources(monitor);
					siCache.hasUpOrDownloaded();
				} else {
					Display.getDefault().asyncExec(new Runnable() {

						@Override
						public void run() {
							MessageDialog.openError(null, "Nuclos Server Project Upload Error",
									"Upload is only allowed if the rule project does not contain any error(s).");
						}
					});
				}
			} finally {
				monitor.done();
				RUNNING.set(null);
			}
		}
	}
	
	private void uploadSources(final IProgressMonitor monitor) throws CoreException {
		final HttpSingleton http = HttpSingleton.getInstance();
		final List<ISourceItem> reloadList = new ArrayList<ISourceItem>();
		try {
			final FileCounter counter = new FileCounter();
			getDirServerrule().accept(counter);
			checkMonitor(monitor);
			if (counter.size() > 0) {
				ticksPerItem = (int) (100 - 10.0) / counter.size();
			} else {
				ticksPerItem = 90;
			}
			monitor.worked(10);
			
			getDirServerrule().accept(new IResourceVisitor() {
				@Override
				public boolean visit(IResource resource) throws CoreException {
					if (resource.exists()) {
						System.out.println(resource.getProjectRelativePath());
						if (resource instanceof IFile) {
							if (resource.getName().endsWith(".java")) {
								final IFile file = (IFile) resource;
								final ISourceItem si = createSourceItem(SourceType.EVENT_SUPPORT_CLASS, file);
								if (siCache.isDirty(si.getQualifiedName())) {
									final ISourceItem newSi;
									try {
										newSi = uploadCodeGen(si, file, monitor);
									} catch (Exception e) {
										final String msg = "uploadSources failed: " + e;
										System.out.println(msg);
										errors.addError(msg);
										return false;
									}
									if (!si.equals(newSi)) {
										reloadList.add(newSi);
									}
								} else {
									System.out.println("Skipping " + si + " because it is not dirty");
								}
							}
						} else {
							return true;
						}
					}
					return true;
				}
			}, IResource.DEPTH_INFINITE, true);
		/*
		} catch (CoreException ex) {
			errors.addError("Nuclos source upload failed: " + ex);
		 */
		} finally {
			monitor.worked(10);
			if (reloadList.size() > 0) {
				ticksPerItem = (int) (100 - 10.0) / reloadList.size();
			} else {
				monitor.worked(90);
			}
			if (!reloadList.isEmpty()) {
				http.forceRuleCompilation(jProject);
				for (ISourceItem si: reloadList) {
					downloadCodeGen(si, monitor);
				}
				NuclosBuilder.justDownloaded(jProject.getProject());
			}
		}
	}
	
	private IFolder getDirServerrule() {
		return jProject.getProject().getFolder(ServerNature.SERVERRULE);
	}
	
	private IFolder getDirBusinessobject() {
		return jProject.getProject().getFolder(ServerNature.BUSINESSOBJECT);
	}
	
	private IFolder getDirStatemodel() {
		return jProject.getProject().getFolder(ServerNature.STATEMODEL);
	}
	
	private IFolder getDirUserrole() {
		return jProject.getProject().getFolder(ServerNature.USERROLE);
	}
	
	private IFolder getDirGeneration() {
		return jProject.getProject().getFolder(ServerNature.OBJECT_GENERATION);
	}
	
	private IFolder getDirReport() {
		return jProject.getProject().getFolder(ServerNature.REPORT);
	}
	
	private IFolder getDirReportDs() {
		return jProject.getProject().getFolder(ServerNature.REPORT_DATASOURCE);
	}
	
	private IFolder getDirImportStructureDefinition() {
		return jProject.getProject().getFolder(ServerNature.IMPORTSTRUCTURE_DEFINITION);
	}
	
	private IFolder getDirPrintout() {
		return jProject.getProject().getFolder(ServerNature.PRINTOUT);
	}
	
	private IFolder getDirParameter() {
		return jProject.getProject().getFolder(ServerNature.PARAMETER);
	}
	
	private IFolder getDirLibs() {
		return jProject.getProject().getFolder(ServerNature.JAVA_LIBS);
	}
	
	private static class FileCounter implements IResourceVisitor {
		
		private int count = 0;
		
		FileCounter() {
		}
		
		@Override
		public boolean visit(IResource resource) throws CoreException {
			if (resource.exists()) {
				if (resource instanceof IFile) {
					++count;
					return false;
				} else {
					return true;
				}
			} else {
				return false;
			}
		}

		public int size() {
			return count;
		}

	}
	
	private static class FileRemover implements IResourceVisitor {
		
		private final IPath root;
		
		private final Set<String> qualifiedNames;
		
		FileRemover(IFolder root, Set<String> qualifiedNames) {
			if (root == null || qualifiedNames == null) {
				throw new NullPointerException();
			}
			this.root = root.getLocation();
			this.qualifiedNames = qualifiedNames;
		}

		@Override
		public boolean visit(IResource resource) throws CoreException {
			final IPath path = resource.getLocation().removeFileExtension();
			if (root.isPrefixOf(path)) {
				if (!(resource instanceof IFile)) {
					return true;
				} else {
					final String qname = path.removeFirstSegments(root.segmentCount())
							.setDevice(null).toPortableString().replace('/', '.');
					if (!qualifiedNames.contains(qname)) {
						resource.delete(ServerNature.FORCE_FILE, null);
					}
					// it's a file, stupid!
					return false;
				}
			} else {
				return false;
			}
		}
		
	}
	
	private static class SourceCounter implements ISourceVisitor {
		
		private int count = 0;
		
		SourceCounter() {
		}

		@Override
		public void process(ISourceItem paramISourceItem) {
			++count;
			for (ISourceItem si: paramISourceItem.getChildren()) {
				process(si);
			}
		}
		
		public int size() {
			return count;
		}

	}
	
	private class SourceItemVisitor implements ISourceVisitor {
		
		private final IProgressMonitor monitor;
		
		private IFolder currentPackageServerrule;
		private IFolder currentPackageBusinessobject;
		private IFolder currentPackageStatemodel;
		private IFolder currentPackageUserrole;
		private IFolder currentPackageGeneration;
		private IFolder currentPackageReport;
		private IFolder currentPackageReportDs;
		private IFolder currentPackageImportStructureDefinition;
		private IFolder currentPackagePrintout;
		private IFolder currentPackageParameter;
		
		private final Set<String> downloadedQn = new HashSet<String>();
		
		SourceItemVisitor(IProgressMonitor monitor) {
			this.monitor = monitor;
		}
		
		private void _process(HttpSingleton http, ISourceItem si) throws CoreException {
			final SourceType type = si.getType();
			final String qname = si.getQualifiedName();
			IFile javaFile;
			
			checkMonitor(monitor);
			monitor.subTask(si.getQualifiedName());
			switch (type) {
				case PACKAGE:
					final String folder = qname.replaceAll("\\.", "/");
					currentPackageServerrule = getDirServerrule().getFolder(folder);
					currentPackageBusinessobject = getDirBusinessobject().getFolder(folder);
					currentPackageStatemodel = getDirStatemodel().getFolder(folder);
					currentPackageUserrole = getDirUserrole().getFolder(folder);
					currentPackageGeneration = getDirGeneration().getFolder(folder);
					currentPackageReport = getDirReport().getFolder(folder);
					currentPackageReportDs = getDirReportDs().getFolder(folder);
					currentPackageImportStructureDefinition = getDirImportStructureDefinition().getFolder(folder);
					currentPackagePrintout = getDirPrintout().getFolder(folder);
					currentPackageParameter = getDirParameter().getFolder(folder);
					break;
				case BO_ENTITIES:
					javaFile = getFile(currentPackageBusinessobject, qname, "java");
					downloadedQn.add(qname);
					if (!uptodate(si, javaFile)) {
						Utils.createFolder(currentPackageBusinessobject);
						http.downloadCodeGen(jProject, javaFile, qname, true);
					}
					break;
				case STATEMODEL:
					javaFile = getFile(currentPackageStatemodel, qname, "java");
					downloadedQn.add(qname);
					if (!uptodate(si, javaFile)) {
						Utils.createFolder(currentPackageStatemodel);
						http.downloadCodeGen(jProject, javaFile, qname, true);
					}
					break;
				case GENERATION:
					javaFile = getFile(currentPackageGeneration, qname, "java");
					downloadedQn.add(qname);
					if (!uptodate(si, javaFile)) {
						Utils.createFolder(currentPackageGeneration);
						http.downloadCodeGen(jProject, javaFile, qname, true);
					}
					break;
				case REPORT:
					javaFile = getFile(currentPackageReport, qname, "java");
					downloadedQn.add(qname);
					if (!uptodate(si, javaFile)) {
						Utils.createFolder(currentPackageReport);
						http.downloadCodeGen(jProject, javaFile, qname, true);
					}
					break;
				case DATASOURCE_REPORT:
					javaFile = getFile(currentPackageReportDs, qname, "java");
					downloadedQn.add(qname);
					if (!uptodate(si, javaFile)) {
						Utils.createFolder(currentPackageReportDs);
						http.downloadCodeGen(jProject, javaFile, qname, true);
					}
					break;
				case IMPORTSTRUCTUREDEFINITIONS:
					javaFile = getFile(currentPackageImportStructureDefinition, qname, "java");
					downloadedQn.add(qname);
					if (!uptodate(si, javaFile)) {
						Utils.createFolder(currentPackageImportStructureDefinition);
						http.downloadCodeGen(jProject, javaFile, qname, true);
					}
					break;
				case PRINTOUT:
					javaFile = getFile(currentPackagePrintout, qname, "java");
					downloadedQn.add(qname);
					if (!uptodate(si, javaFile)) {
						Utils.createFolder(currentPackagePrintout);
						http.downloadCodeGen(jProject, javaFile, qname, true);
						downloadedQn.add(qname);
					}
					break;
				case PARAMETER:
					javaFile = getFile(currentPackageParameter, qname, "java");
					downloadedQn.add(qname);
					if (!uptodate(si, javaFile)) {
						Utils.createFolder(currentPackageParameter);
						http.downloadCodeGen(jProject, javaFile, qname, true);
					}
					break;
				case USERROLE:
					javaFile = getFile(currentPackageUserrole, qname, "java");
					downloadedQn.add(qname);
					if (!uptodate(si, javaFile)) {
						Utils.createFolder(currentPackageUserrole);
						http.downloadCodeGen(jProject, javaFile, qname, true);
					}
					break;
				case EVENT_SUPPORT_CLASS:
					javaFile = getFile(currentPackageServerrule, qname, "java");
					downloadedQn.add(qname);
					if (!uptodate(si, javaFile)) {
						Utils.createFolder(currentPackageServerrule);
						http.downloadCodeGen(jProject, javaFile, qname, false);
					}
					break;
				
				case CODE_COMPILER_CLASS_EXTENSION:
					// do nothing
					System.out.println("nothing to do for: " + si);
					break;
				default:
					// do nothing
					System.out.println("nothing to do for unknown source type: " + si);
			}
			monitor.worked(ticksPerItem);
			
			// next 2 line MUST be done AFTER the uptodate check (tp)
			siCache.addOrUpdate(si);
			siCache.notDirty(si.getQualifiedName());
			
			processChildren(si);
		}
		
		private boolean uptodate(ISourceItem fromServer, IFile javaFile) {
			final String qualifiedName = fromServer.getQualifiedName();
			if (!javaFile.exists() || siCache.isDirty(qualifiedName)) {
				return false;
			}
			final ISourceItem local = siCache.getSourceItem(qualifiedName);
			if (local == null) {
				return false;
			}
			final boolean result = fromServer.getHashValue() != null && fromServer.equals(local);
			if (result) {
				System.out.println(qualifiedName + " is up-to-date (not downloading)");
			}
			return result;
		}
		
		private void processChildren(ISourceItem si) throws CoreException {
			final HttpSingleton http = HttpSingleton.getInstance();
			if (si.getChildren() != null) {
				for (ISourceItem child : si.getChildren()) {
					_process(http, child);
				}
			}
		}
		
		/**
		 * Use interface here
		 */
		@Override
		public void process(ISourceItem si) {
			final HttpSingleton http = HttpSingleton.getInstance();
			try {
				_process(http, si);
			} catch (final CoreException e) {
				Display.getDefault().asyncExec(new Runnable() {
					
					@Override
					public void run() {
						ExceptionHandler.handle(e, "Nuclos Server Transfer: SourceItemVisitor._process(..) failed", e.toString());
					}
				});
			}
		}
		
		public Set<String> getDownloadedItems() {
			return downloadedQn;
		}
		
	}
	
	private ISourceItem createSourceItem(SourceType type, IFile file) {
		final IPath rpath = file.getProjectRelativePath();
		String qualifiedName = 
				rpath
				.removeFirstSegments(1)
				.removeLastSegments(1)
				.removeTrailingSeparator()
				.toString()
				.replace('/', '.') + "." +
				file.getName().substring(0, file.getName().length()-".java".length());
		// fix for default package
		if (qualifiedName.startsWith(".")) {
			qualifiedName = qualifiedName.substring(1);
		}
		final SourceItem result = new SourceItem();
		result.setQualifiedName(qualifiedName);
		result.setType(SourceType.EVENT_SUPPORT_CLASS);
		
		// retrieve cached source item
		final ISourceItem cached = siCache.getSourceItem(qualifiedName);
		if (cached != null) {
			result.setHashValue(cached.getHashValue());
			result.setId(cached.getId());
			result.setVersion(cached.getVersion());
			siCache.addOrUpdate(result);
		}
		
		return result;
	}
	
	private ISourceItem uploadCodeGen(ISourceItem si, IFile file, IProgressMonitor monitor) throws CoreException {
		final HttpSingleton http = HttpSingleton.getInstance();
		
		checkMonitor(monitor);
		monitor.subTask(si.getQualifiedName());
		
		try {
			final InputStream is = file.getContents(true);
			final ByteArrayOutputStream buffer = new ByteArrayOutputStream();
			try {
				int nRead;
				final byte[] data = new byte[16384];
				while ((nRead = is.read(data, 0, data.length)) >= 0) {
					buffer.write(data, 0, nRead);
				}
			} finally {
				buffer.close();
			}
			byte[] content = buffer.toByteArray();
			
			final String hashValue = MessageDigest.digestAsBase64(ServerNature.DIGEST, content);
		
			System.out.println("Uploading " + si);
			
			if (!hashValue.equals(si.getHashValue())) {
				final ISourceItem result = http.uploadSourceItem(jProject, si, false, content);
				errors.showOnErrors();
				errors.clear();
				monitor.worked(ticksPerItem);
				
				// use hash calculated here - it's the most recent one (tp)
				si.setHashValue(hashValue);
				siCache.addOrUpdate(si);
				
				return result;
			} else {
				System.out.println("Skipping " + si + " because hash has not changed");
				return si;
			}
		} catch (IOException e) {
			throw new NuclosFatalIdeException(e);
		} catch (SecurityException e) {
			throw new NuclosFatalIdeException(e);
		}
	}
	
	private boolean downloadCodeGen(ISourceItem item, IProgressMonitor monitor) throws CoreException {
		final HttpSingleton http = HttpSingleton.getInstance();
		final String path = item.getQualifiedName().replace('.', File.separatorChar);
		final int idx = path.lastIndexOf(File.separatorChar);
		final IFolder fPackage;
		if (idx >= 0) {
			fPackage = getDirServerrule().getFolder(path.substring(0, idx));
		} else {
			fPackage = getDirServerrule();
		}
		checkMonitor(monitor);
		monitor.subTask(item.getQualifiedName());
		final IFile javaFile = getFile(fPackage, item.getQualifiedName(), "java");
		final boolean result = http.downloadCodeGen(jProject, javaFile, item.getQualifiedName(), false);
		monitor.worked(ticksPerItem);
		return result;
	}
	
	private void readTree(ISourceVisitor visitor, IProgressMonitor monitor) throws CoreException {
		final HttpSingleton http = HttpSingleton.getInstance();
		try {
			final ISourceItem si = http.getTree(jProject, null, null, false);
			errors.showOnErrors();
			errors.clear();
			
			checkMonitor(monitor);
			monitor.worked(5);
			final SourceCounter counter = new SourceCounter();
			counter.process(si);
			checkMonitor(monitor);
			if (counter.size() > 0) {
				ticksPerItem = (int) (100 - 10.0) / counter.size();
				monitor.worked(5);
			} else {
				monitor.worked(95);
			}
			visitor.process(si);
		} catch (SecurityException e) {
			throw new NuclosFatalIdeException(e);
		}
	}
	
	public void download(IProgressMonitor monitor) {
		final IProject project = jProject.getProject();
		if (RUNNING.compareAndSet(null, project)) {
			try {
				// Sometimes CCCE.jar has gone corrupt. To 'heal' the project, we reinstall the API. (tp)
				// see also http://support.novabit.de/browse/RSWORGA-89
				checkMonitor(monitor);
				monitor.beginTask("downloading...", 200);
				installNuclosApi(monitor);
				checkMonitor(monitor);
				downloadSources(monitor);
				siCache.hasUpOrDownloaded();
			} finally {
				monitor.done();
				RUNNING.set(null);
			}
		}
	}
	
	private static void checkMonitor(IProgressMonitor monitor) {
		if (monitor.isCanceled()) {
			throw new OperationCanceledException();
		}
	}
	
	private void installNuclosApi(IProgressMonitor monitor) {
		removeOldLibs();
		final HttpSingleton http = HttpSingleton.getInstance();
		try {
			final INuclosApi api = http.getNuclosApi(jProject);
			System.out.println("Nuclos API Jars: " + api.getNuclosApiJars());
			System.out.println("Nuclos Version: " + api.getNuclosVersion());
			jProject.getProject().setPersistentProperty(ServerNature.PROPERTY_NUCLOS_VERSION, api.getNuclosVersion());
			Set<String> jars = new HashSet<String>(api.getNuclosApiJars());
			
			checkMonitor(monitor);
			monitor.worked(10);
			ticksPerItem = (int) (100 - 20.0) / (jars.size() + 1);
			
			// download the jars
			for (String jarFileName : jars) {
				checkMonitor(monitor);
				monitor.subTask(jarFileName);
				http.downloadApiJar(jProject, getDirLibs(), jarFileName);
				monitor.worked(ticksPerItem);
			}
			checkMonitor(monitor);
			monitor.subTask("CCCE.jar");
			final IFile ccceJar = getFile(getDirLibs(), "CCCE", "jar");
			if (http.downloadCodeGen(jProject, ccceJar, "CCCE", true)) {
				jars.add("CCCE.jar");
				monitor.worked(ticksPerItem);
			}
			/*
			if (downloadCodeGen(getDirLibs(), "ReportDSEntities", "jar", false)) {
				jars.add("ReportDSEntities.jar");
			}
			 */
			
			// extend class path
			List<IClasspathEntry> entries = new ArrayList<IClasspathEntry>();
			for (String jarFileName : jars) {
				entries.add(JavaCore.newLibraryEntry(getDirLibs().getFile(jarFileName).getFullPath(), null, null));
			}
			
			final Set<IClasspathEntry> es = new TreeSet<IClasspathEntry>(ClasspathEntryComparator.getInstance());
			for (IClasspathEntry e: jProject.getRawClasspath()) {
				es.add(e);
			}
			es.addAll(entries);
			
			/* debugging 
			System.out.println("Read .classpath:");
			IClasspathEntry[] cpes = jProject.readRawClasspath();
			for (IClasspathEntry c: cpes) {
				System.out.println(c);
			}
			System.out.println("New classpath set:");
			for (IClasspathEntry c: es) {
				System.out.println(c);
			}
			 */
			
			// keep original sequence
			entries.clear();
			for (IClasspathEntry e: jProject.getRawClasspath()) {
				if (es.contains(e)) {
					entries.add(e);
					es.remove(e);
				}
			}
			for (IClasspathEntry e: es) {
				entries.add(e);
			}
			
			// finally set new classpath
			jProject.setRawClasspath(entries.toArray(EMPTY_CPE_ARRAY), null);
			checkMonitor(monitor);
			monitor.worked(10);
		} catch (CoreException e) {
			/*
			IStatus warning = new Status(IStatus.ERROR, Activator.PLUGIN_ID, 1, "Nuclos API installation failed", ex);
			ErrorDialog.openError(null, "Nuclos IDE", null, warning);
			throw new NuclosFatalIdeException(ex);
			 */
			errors.addError("Nuclos API installation failed: " + e);
		} catch (SecurityException e) {
			errors.addError("Nuclos API installation failed: " + e);			
		}
	}
	
	private void removeOldLibs() {
		final IFolder folder = getDirLibs();
		try {
			folder.delete(ServerNature.FORCE_FOLDER, null);
		} catch (CoreException e) {
			// ignore
		}
		try {
			folder.create(ServerNature.FORCE_FOLDER, true, null);
		} catch (CoreException e) {
			// ignore
		}
	}
	
	private IFile getFile(IFolder fPackage, String qualifiedName, String fileType) {
		final String name;
		if (qualifiedName.contains(".")) {
			name = qualifiedName.substring(qualifiedName.lastIndexOf(".") + 1);
		} else {
			name = qualifiedName;
		}
		final String filename = name + "." + fileType;
		final IFile fJavaSource = fPackage.getFile(filename);
		return fJavaSource;
	}
	
	public static boolean isRunning() {
		return RUNNING.get() != null;
	}
	
	public static boolean isRunningOn(IProject ip) {
		if (ip == null) {
			throw new NullPointerException();
		}
		final IProject p = RUNNING.get();
		return ip.equals(p);
	}
	
}
