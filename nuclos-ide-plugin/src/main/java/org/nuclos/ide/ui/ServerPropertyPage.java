//Copyright (C) 2014  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.ide.ui;

import org.apache.http.util.LangUtils;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.internal.ui.util.ExceptionHandler;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.dialogs.PropertyPage;
import org.nuclos.ide.server.ServerDebug;
import org.nuclos.ide.server.ServerDownload;
import org.nuclos.ide.server.ServerNature;
import org.nuclos.ide.server.ServerUpload;

public class ServerPropertyPage extends PropertyPage implements KeyListener {
	
	// GUI elements
	
	private Text txtHost = null;
	private Text txtPort = null;
	private Text txtInstance = null;
	private Text txtDebugPort = null;
	private Button oldRules = null;
	private Label lblNuclosVersion = null;
	
	private Button download;
	private Button upload;
	
	// Page state
	
	private boolean valid = true;
	
	// other 
	
	private MouseListener downloadListener;
	
	private MouseListener uploadListener;
	
	public ServerPropertyPage() {
	}

	@Override
	protected Control createContents(Composite parent) {
		Composite container = new Composite(parent, SWT.NULL);
		GridLayout layout = new GridLayout();
		container.setLayout(layout);
		layout.numColumns = 2;
		GridData gd = new GridData(GridData.FILL_HORIZONTAL);
		
		new Label(container, SWT.NULL).setText("Host");
		txtHost = new Text(container, SWT.BORDER | SWT.SINGLE);
		txtHost.addKeyListener(this);
		txtHost.setLayoutData(gd);
		
		new Label(container, SWT.NULL).setText("Port");
		txtPort = new Text(container, SWT.BORDER | SWT.SINGLE);
		txtPort.addKeyListener(this);
		txtPort.setLayoutData(gd);
		
		new Label(container, SWT.NULL).setText("Instance");
		txtInstance = new Text(container, SWT.BORDER | SWT.SINGLE);
		txtInstance.addKeyListener(this);
		txtInstance.setLayoutData(gd);
		
		new Label(container, SWT.NULL).setText("Debug Port");
		txtDebugPort = new Text(container, SWT.BORDER | SWT.SINGLE);
		txtDebugPort.addKeyListener(this);
		txtDebugPort.setLayoutData(gd);
		
		new Label(container, SWT.NULL).setText("Old rules");
		oldRules = new Button(container, SWT.CHECK);
		oldRules.setText("setting is read-only");
		oldRules.addKeyListener(this);
		oldRules.setLayoutData(gd);
		oldRules.setEnabled(false);
		oldRules.setGrayed(true);
		
		new Label(container, SWT.NULL).setText("Version");
		lblNuclosVersion = new Label(container, SWT.NULL);
		
		try {
			downloadListener = new ServerDownload(getJavaProject());
			uploadListener = new ServerUpload(getJavaProject());
		} catch (CoreException e) {
			ExceptionHandler.handle(e, "Nuclos Server Property Page: getJavaProject() failed", e.toString());
		}
		
		new Label(container, SWT.NULL).setText("Nuclos Rules");
		download = new Button(container, SWT.PUSH);
		download.setText("Download Sources");
		download.addMouseListener(downloadListener);
		download.setEnabled(true);
		download.setLayoutData(gd);
		new Label(container, SWT.NULL).setText("");
		upload = new Button(container, SWT.PUSH);
		upload.setText("Upload Sources");
		upload.addMouseListener(uploadListener);
		upload.setEnabled(true);
		upload.setLayoutData(gd);
		
		try {
			initializeValues();
		} catch (CoreException e) {
			ExceptionHandler.handle(e, "Nuclos Server Property Page: initializeValues() failed", e.toString());
		}	
		return container;
	}
	
	private void initializeValues() throws CoreException {
		IResource resource = getProject();
		String host = resource.getPersistentProperty(ServerNature.PROPERTY_HOST);
		if (host != null) {
			txtHost.setText(host);
		}
		String port = resource.getPersistentProperty(ServerNature.PROPERTY_PORT);
		if (port != null) {
			txtPort.setText(port);
		}
		String instance = resource.getPersistentProperty(ServerNature.PROPERTY_INSTANCE);
		if (instance != null) {
			txtInstance.setText(instance);
		}
		String debugPort = resource.getPersistentProperty(ServerNature.PROPERTY_DEBUG_PORT);
		if (debugPort != null) {
			txtDebugPort.setText(debugPort);
		}
		Boolean or = Boolean.valueOf(resource.getPersistentProperty(ServerNature.PROPERTY_OLD_RULES));
		oldRules.setSelection(or);
		String nuclosVersion = resource.getPersistentProperty(ServerNature.PROPERTY_NUCLOS_VERSION);
		if (nuclosVersion != null) {
			lblNuclosVersion.setText(nuclosVersion);
		}
	}

	private void storeValues() throws CoreException {
		IResource resource = getProject();
		boolean hostChanged = LangUtils.equals(
				resource.getPersistentProperty(ServerNature.PROPERTY_HOST), 
				txtHost.getText());
		boolean debugChanged = LangUtils.equals(
				resource.getPersistentProperty(ServerNature.PROPERTY_DEBUG_PORT), 
				txtDebugPort.getText());
		boolean oldRulesChanged = LangUtils.equals(
				resource.getPersistentProperty(ServerNature.PROPERTY_OLD_RULES), 
				oldRules.getSelection());
		
		resource.setPersistentProperty(ServerNature.PROPERTY_HOST, txtHost.getText());
		resource.setPersistentProperty(ServerNature.PROPERTY_PORT, txtPort.getText());
		resource.setPersistentProperty(ServerNature.PROPERTY_INSTANCE, txtInstance.getText());
		resource.setPersistentProperty(ServerNature.PROPERTY_DEBUG_PORT, txtDebugPort.getText());
		resource.setPersistentProperty(ServerNature.PROPERTY_OLD_RULES, Boolean.toString(oldRules.getSelection()));
		
		if (hostChanged || debugChanged) {
			ServerDebug.updateConfiguration(getProject());
		}
	}
	
	private IProject getProject() {
		final IAdaptable p = getElement();
		if (p instanceof IJavaProject) {
			return ((IJavaProject) p).getProject();
		} else {
			return (IProject) getElement();
		}
	}
	
	private IJavaProject getJavaProject() throws CoreException {
		final IAdaptable p = getElement();
		if (p instanceof IJavaProject) {
			return (IJavaProject) getElement();
		} else {
			final IProject project = (IProject) p;
			// http://stackoverflow.com/questions/6642704/cannot-cast-eclipse-project-to-ijavaproject
			if (project.hasNature(JavaCore.NATURE_ID)) {
		        return JavaCore.create(project);
		    }
			throw new IllegalStateException("Unknown element type " + p);
		}
	}

	@Override
	public boolean performOk() {
		if (!valid) {
			return false;
		}
		boolean result = super.performOk();
		if (result) {
			try {
				storeValues();
			} catch (CoreException e1) {
				e1.printStackTrace();
			}
		}
		return result;
	}

	@Override
	public void keyPressed(KeyEvent e) {
	}

	@Override
	public void keyReleased(KeyEvent e) {
		valid = true;
		if (txtHost.getText().isEmpty()
				|| txtPort.getText().isEmpty() 
				|| txtInstance.getText().isEmpty()
				|| txtDebugPort.getText().isEmpty()) {
			valid = false;
		}
	}

}
