//Copyright (C) 2014  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.ide;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.WritableByteChannel;

import org.eclipse.core.resources.ICommand;
import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.nuclos.ide.server.NuclosBuilder;
import org.nuclos.ide.server.ServerNature;

public class Utils {

	private Utils() {
		// Never invoked.
	}

	public static IJavaProject getSelectedProject() {
		IWorkbenchWindow window = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
		if (window != null)
		{
			IStructuredSelection selection = (IStructuredSelection) window.getSelectionService().getSelection();
			Object firstElement = selection.getFirstElement();
			if (firstElement instanceof IAdaptable)
			{
				IJavaProject project = (IJavaProject) ((IAdaptable) firstElement).getAdapter(IJavaProject.class);
				return project;
			}
		}

		return null;
	}

	public static void createFolder(IFolder folder) throws CoreException {
		if (folder.exists()) {
			return;
		}
		IContainer parent = folder.getParent();
		if (parent instanceof IFolder) {
			createFolder((IFolder) parent);
		}
		if (!folder.exists()) {
			folder.create(ServerNature.FORCE_FOLDER, true, null);
		}
	}

	public static void updateOrCreate(IFile file, InputStream content) throws CoreException {
		if (file.exists()) {
			file.setContents(content, ServerNature.FORCE_FILE, true, null);
		} else {
			file.create(content, ServerNature.FORCE_FILE, null);
		}
	}

	public static void ensureBuilder(IProject project) throws CoreException {
		if (!project.hasNature(ServerNature.NATURE_ID)) {
			throw new NuclosFatalIdeException("Not a Nuclos Server Rules Project");
		}

		IProjectDescription desc = project.getDescription();
		if (desc == null) {
			desc = ResourcesPlugin.getWorkspace().newProjectDescription(project.getName());
		}
		final ICommand[] commands = desc.getBuildSpec();
		boolean found = false;

		for (int i = 0; i < commands.length; ++i) {
			if (commands[i].getBuilderName().equals(NuclosBuilder.BUILDER_ID)) {
				found = true;
				break;
			}
		}
		if (!found) {
			//add builder to project
			final ICommand command = desc.newCommand();
			command.setBuilderName(NuclosBuilder.BUILDER_ID);
			final ICommand[] newCommands = new ICommand[commands.length + 1];

			// Add it before other builders.
			System.arraycopy(commands, 0, newCommands, 0, commands.length);
			newCommands[commands.length] = command;
			desc.setBuildSpec(newCommands);
			project.setDescription(desc, null);
		}
	}

	public static void removeBuilder(IProject project) throws CoreException {
		final IProjectDescription desc = project.getDescription();
		final ICommand[] commands = desc.getBuildSpec();
		boolean found = false;

		int i;
		for (i = 0; i < commands.length; ++i) {
			if (commands[i].getBuilderName().equals(NuclosBuilder.BUILDER_ID)) {
				found = true;
				break;
			}
		}
		if (!found) {
			//remove builder from project
			final ICommand[] newCommands = new ICommand[commands.length - 1];
			System.arraycopy(commands, 0, newCommands, 0, i);
			System.arraycopy(commands, i + 1, newCommands, i, commands.length - i - 1);

			desc.setBuildSpec(newCommands);
			project.setDescription(desc, null);
		}
	}

	/**
	 * http://thomaswabner.wordpress.com/2007/10/09/fast-stream-copy-using-javanio-channels/
	 */
	public static void copy(final ReadableByteChannel src, final WritableByteChannel dest)
			throws IOException {
		final ByteBuffer buffer = ByteBuffer.allocateDirect(16 * 1024);
		while (src.read(buffer) != -1) {
			// prepare the buffer to be drained
			buffer.flip();
			// write to the channel, may block
			dest.write(buffer);
			// If partial transfer, shift remainder down
			// If buffer is empty, same as doing clear()
			buffer.compact();
		}
		// EOF will leave buffer in fill state
		buffer.flip();
		// make sure the buffer is fully drained.
		while (buffer.hasRemaining()) {
			dest.write(buffer);
		}
	}
	
	public static void copy(InputStream in, OutputStream out) throws IOException {
		try {
			copy(Channels.newChannel(in), Channels.newChannel(out));
		} finally {
			out.close();
			in.close();
		}
	}
}
