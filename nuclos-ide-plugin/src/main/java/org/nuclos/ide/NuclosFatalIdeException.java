//Copyright (C) 2014  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.ide;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;

/**
 * Base exception for fatal errors in Nuclos eclipse plugin. 
 * 
 * @author Thomas Pasch
 */
public class NuclosFatalIdeException extends CoreException {

	/**
	 * @param tCause wrapped exception
	 * @postcondition this.getMessage().equals(tCause.getMessage())
	 */
	public NuclosFatalIdeException(Throwable tCause) {
		super(new Status(IStatus.ERROR, Activator.PLUGIN_ID, tCause.toString(), tCause));
	}
	
	/**
	 * @param sMessage exception message
	 */
	public NuclosFatalIdeException(String sMessage) {
		super(new Status(IStatus.ERROR, Activator.PLUGIN_ID, sMessage));
	}
	
	/**
	 * @param sMessage exception message
	 * @param tCause wrapped exception
	 */
	public NuclosFatalIdeException(String sMessage, Throwable tCause) {
		super(new Status(IStatus.ERROR, Activator.PLUGIN_ID, sMessage, tCause));
	}
	
}
