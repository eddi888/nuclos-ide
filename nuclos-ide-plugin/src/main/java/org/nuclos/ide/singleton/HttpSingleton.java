//Copyright (C) 2014  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.ide.singleton;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URI;
import java.net.URISyntaxException;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.HttpHostConnectException;
import org.apache.http.entity.BasicHttpEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.QualifiedName;
import org.eclipse.jdt.core.IJavaProject;
import org.nuclos.api.ide.SourceItemFacadeRemote;
import org.nuclos.api.ide.valueobject.INuclosApi;
import org.nuclos.api.ide.valueobject.ISourceItem;
import org.nuclos.api.ide.valueobject.SourceType;
import org.nuclos.digest.MessageDigestInputStream;
import org.nuclos.ide.NuclosFatalIdeException;
import org.nuclos.ide.server.ServerNature;
import org.springframework.remoting.support.RemoteInvocation;
import org.springframework.remoting.support.RemoteInvocationResult;

/**
 * Central singleton for low-level (http and spring rpc) communication 
 * with Nuclos server.
 * 
 * @author Thomas Pasch
 */
public class HttpSingleton {
	
	private static final HttpSingleton INSTANCE = new HttpSingleton();
	
	//
	
	private HttpClient httpClient;
	
	private HttpSingleton() {
	}
	
	public static final HttpSingleton getInstance() {
		return INSTANCE;
	}
	
	private HttpClient getHttpClient() {
		if (httpClient == null) {
			httpClient = new DefaultHttpClient();
		}
		return httpClient;
	}

	public void forceRuleCompilation(IJavaProject jProject) throws CoreException {
		try {
			final HttpPost httpPost = new HttpPost(new URI(getConnectionUrl(jProject, "remoting/SourceItemService")));
			
			final Method forceRuleCompilation = SourceItemFacadeRemote.class.getDeclaredMethod(
					"forceRuleCompilation", new Class<?>[0]);
			
			invokeOnServer(jProject.getProject(), httpPost, forceRuleCompilation);
		} catch (NoSuchMethodException e) {
			throw new NuclosFatalIdeException(e);
		} catch (URISyntaxException e) {
			throw new NuclosFatalIdeException(e);
		} 
	}
	
	public INuclosApi getNuclosApi(IJavaProject jProject) throws CoreException {
		try {
			final HttpPost httpPost = new HttpPost(new URI(getConnectionUrl(jProject, "remoting/SourceItemService")));
			final Method getNuclosApi = SourceItemFacadeRemote.class.getDeclaredMethod("getNuclosApi");
			
			final INuclosApi result = extract(invokeOnServer(jProject.getProject(), httpPost, getNuclosApi), INuclosApi.class);
			return result;
		} catch (NoSuchMethodException e) {
			throw new NuclosFatalIdeException(e);
		} catch (URISyntaxException e) {
			throw new NuclosFatalIdeException(e);
		} catch (ClassNotFoundException e) {
			throw new NuclosFatalIdeException(e);
		} catch (IOException e) {
			throw new NuclosFatalIdeException(e);
		} 
	}
	
	public ISourceItem getTree(IJavaProject jProject, SourceType type, String qualifiedPackagenameAsRoot, boolean includeOldApiRules) throws CoreException {
		try {
			final HttpPost httpPost = new HttpPost(new URI(getConnectionUrl(jProject, "remoting/SourceItemService")));			
			final Method getTree = SourceItemFacadeRemote.class.getDeclaredMethod("getTree", 
					new Class<?>[] { SourceType.class, String.class, boolean.class });
			
			final ISourceItem result = extract(invokeOnServer(
					jProject.getProject(), httpPost, getTree, type, qualifiedPackagenameAsRoot, includeOldApiRules), ISourceItem.class);
			return result;
		} catch (NoSuchMethodException e) {
			throw new NuclosFatalIdeException(e);
		} catch (URISyntaxException e) {
			throw new NuclosFatalIdeException(e);
		} catch (ClassNotFoundException e) {
			throw new NuclosFatalIdeException(e);
		} catch (IOException e) {
			throw new NuclosFatalIdeException(e);
		} 
	}
	
	public ISourceItem uploadSourceItem(IJavaProject jProject, ISourceItem item, boolean isOldRule, byte[] content) throws CoreException {
		try {
			final HttpPost httpPost = new HttpPost(new URI(getConnectionUrl(jProject, "remoting/SourceItemService")));			
			final Method uploadSourceItem = SourceItemFacadeRemote.class.getDeclaredMethod("uploadSourceItem", 
					new Class<?>[] { ISourceItem.class, boolean.class, byte[].class });
			
			final ISourceItem result = extract(invokeOnServer(
					jProject.getProject(), httpPost, uploadSourceItem, item, isOldRule, content), ISourceItem.class);
			return result;
		} catch (NoSuchMethodException e) {
			throw new NuclosFatalIdeException(e);
		} catch (URISyntaxException e) {
			throw new NuclosFatalIdeException(e);
		} catch (ClassNotFoundException e) {
			throw new NuclosFatalIdeException(e);
		} catch (IOException e) {
			throw new NuclosFatalIdeException(e);
		} 
	}
	
	private RemoteInvocationResult invokeOnServer(IProject project, HttpPost httpPost, Method method, Object... arguments) throws CoreException {
		try {
			// SourceType type, String qualifiedName, boolean isOldRule, byte[] content
			final HttpEntity httpEntitiy = invokeEntity(method, arguments);
			httpPost.setEntity(httpEntitiy);

			// The underlying HTTP connection is still held by the response object 
			// to allow the response content to be streamed directly from the network socket. 
			// In order to ensure correct deallocation of system resources 
			// the user MUST either fully consume the response content  or abort request 
			// execution by calling HttpGet#releaseConnection().

			try {
				HttpResponse response1 = getHttpClient().execute(httpPost);
				System.out.println("status: " + response1.getStatusLine());
				for (Header h : response1.getAllHeaders()) {
					System.out.println("header: " + h);
				}
				HttpEntity entity1 = response1.getEntity();
				long len = entity1.getContentLength();
				System.out.println("length: " + len);
				System.out.println("type: " + entity1.getContentType());
				if (response1.getStatusLine().getStatusCode() != 200) {
					// throw new IOException("Server error: " + response1.getStatusLine());
					Errors.getInstance(project).addError("Nuclos server error: " + response1.getStatusLine() + " on " + httpPost);
				}
				
				final ObjectInputStream in = new ObjectInputStream(entity1.getContent());
				final RemoteInvocationResult result;
				try {
					result = (RemoteInvocationResult) in.readObject();
					if (in.read() >= 0) {
						throw new IllegalStateException();
					}
				} finally {
					in.close();
				}
				// ensure it is fully consumed
				EntityUtils.consume(entity1);
				return result;
			} finally {
				httpPost.releaseConnection();
			}
		} catch (IOException e) {
			throw new NuclosFatalIdeException(e);
		} catch (SecurityException e) {
			throw new NuclosFatalIdeException(e);
		} catch (ClassNotFoundException e) {
			throw new NuclosFatalIdeException(e);
		}
	}
	
	public void downloadApiJar(IJavaProject jProject, IFolder libsDir, String jarFileName) throws CoreException {
		try {
			URI uri = new URI(getConnectionUrl(jProject, "apidownload/")+jarFileName);
			IFile fJavaSource = libsDir.getFile(jarFileName);
			
			// see https://hc.apache.org/httpcomponents-client-ga/quickstart.html
			final HttpGet httpGet = new HttpGet(uri);
	
			// The underlying HTTP connection is still held by the response object 
			// to allow the response content to be streamed directly from the network socket. 
			// In order to ensure correct deallocation of system resources 
			// the user MUST either fully consume the response content  or abort request 
			// execution by calling HttpGet#releaseConnection().
	
			try {
				HttpResponse response1 = getHttpClient().execute(httpGet);
				System.out.println("status: " + response1.getStatusLine());
				for (Header h : response1.getAllHeaders()) {
					System.out.println("header: " + h);
				}
				HttpEntity entity1 = response1.getEntity();
				
				/*
				if (fJavaSource.exists()) {
					try {
						fJavaSource.delete(true, null);
					}
					catch (CoreException e) {
						// ignore
					}
				}
				 */
				if (response1.getStatusLine().getStatusCode() == 200) {
					if (fJavaSource.exists()) {
						fJavaSource.setContents(entity1.getContent(), ServerNature.FORCE_FILE, true, null);
					} else {
						fJavaSource.create(entity1.getContent(), ServerNature.FORCE_FILE, null);
					}
					fJavaSource.setDerived(true, null);
				} else {
					// throw new IOException("Server error: " + response1.getStatusLine());
					Errors.getInstance(jProject.getProject()).addError(
							"Nuclos server error: " + response1.getStatusLine() + " on " + httpGet);
				}
				// ensure it is fully consumed
				EntityUtils.consume(entity1);
			} finally {
				httpGet.releaseConnection();
			}
		} catch (IOException ex) {
			throw new NuclosFatalIdeException(ex);
		} catch (URISyntaxException ex) {
			throw new NuclosFatalIdeException(ex);			
		}
	}
	
	public boolean downloadCodeGen(IJavaProject jProject, final IFile fJavaSource, final String qualifiedName,
			boolean derived) throws CoreException
	{
		final IProject project = jProject.getProject();
		boolean succeeded = false;
		try {
			URI uri = new URI(getConnectionUrl(jProject, "sidownload/") + qualifiedName.replaceAll("\\.", "/") 
					+ "." + fJavaSource.getFileExtension());
			final HttpGet httpGet = new HttpGet(uri);

			// The underlying HTTP connection is still held by the response object 
			// to allow the response content to be streamed directly from the network socket. 
			// In order to ensure correct deallocation of system resources 
			// the user MUST either fully consume the response content  or abort request 
			// execution by calling HttpGet#releaseConnection().

			try {
				final HttpResponse response1 = getHttpClient().execute(httpGet);
				System.out.println("status: " + response1.getStatusLine());
				for (Header h : response1.getAllHeaders()) {
					System.out.println("header: " + h);
				}
				final HttpEntity entity1 = response1.getEntity();

				if (response1.getStatusLine().getStatusCode() == 200) {
					final MessageDigestInputStream in = new MessageDigestInputStream(entity1.getContent(), ServerNature.DIGEST);
					try {
						if (fJavaSource.exists()) {
							fJavaSource.setContents(in, ServerNature.FORCE_FILE, true, null);
						} else {
							fJavaSource.create(in, ServerNature.FORCE_FILE, null);
						}
					} finally {
						in.close();
					}
					final String hash = in.digestAsBase64();
					/*
					 * TODO:
					 * The received and written hashValue differs. But why?
					 * (tp) 
					 */
					/*
					final String hash2 = MessageDigest.digestAsBase64(ServerNature.DIGEST, fJavaSource.getContents(true));
					if (!hash.equals(hash2)) {
						System.out.println("hash1=" + hash + " hash2=" + hash2);
					}
					 */
					SourceItemCache.getInstance(project).receivedDigest(qualifiedName, hash);
					if (derived) {
						fJavaSource.setDerived(true, null);
					}
					// Sometimes, I get a BackingStoreException here, thus commenting this out... (tp)
					// fJavaSource.setCharset(ServerNature.DEFAULT_CHARSET, null);
					succeeded = true;
				} else {
					Errors.getInstance(project).addError(
							"Nuclos server error: " + response1.getStatusLine() + " on " + httpGet);
					succeeded = false;
				}
				// ensure it is fully consumed
				EntityUtils.consume(entity1);
			} finally {
				httpGet.releaseConnection();
			}
		} catch (HttpHostConnectException e) {
			succeeded = false;
			Errors.getInstance(project).addError("Nuclos server error: (Is the server really up?): " + e);
		} catch (IOException e) {
			throw new NuclosFatalIdeException(e);
		} catch (URISyntaxException e) {
			throw new NuclosFatalIdeException(e);
		}
		return succeeded;
	}
		
	public String testConnection(IProject project, String host, String port, String instance) {
		final Errors errors = Errors.getInstance(project);
		errors.clear();
		try {
			final HttpPost httpPost = new HttpPost(new URI(getConnectionUrl(host, port, instance, "remoting/SourceItemService")));
			final Method getNuclosApi = SourceItemFacadeRemote.class.getDeclaredMethod("getNuclosApi");
			
			final RemoteInvocationResult result = invokeOnServer(project, httpPost, getNuclosApi);
			final Throwable ex = result.getException();
			if (ex != null) {
				return "Server returns: " + getMessageFromThrowable(ex);
			}
			errors.throwOnErrors();
		} catch (Exception ex) {
			// ok, only for test connection
			return ex.toString();
		}
		return null;
	}
	
	private String getConnectionUrl(IJavaProject jProject, String path) throws CoreException {
		return getConnectionUrl(
				getProjectProperty(jProject, ServerNature.PROPERTY_HOST, "localhost"),
				getProjectProperty(jProject, ServerNature.PROPERTY_PORT, "8080"),
				getProjectProperty(jProject, ServerNature.PROPERTY_INSTANCE, "nuclos-war"),
				path);
	}
	
	private String getProjectProperty(IJavaProject jProject, QualifiedName id, String sDefault) throws CoreException {
		String result = jProject.getProject().getPersistentProperty(id);
		if (result == null) {
			result = sDefault;
		}
		return result;
	}
	
	private static String getConnectionUrl(String host, String port, String instance, String path) {
		StringBuffer result = new StringBuffer();
		result.append("http://");
		result.append(host);
		result.append(":");
		result.append(port);
		result.append("/");
		result.append(instance);
		result.append("/");
		if (path != null) {
			result.append(path);
		}
		return result.toString();
	}
	
	private static HttpEntity invokeEntity(Method method, Object... arguments) throws IOException {
		final BasicHttpEntity httpEntitiy = new BasicHttpEntity();
		final RemoteInvocation ri = new RemoteInvocation();
		ri.setParameterTypes(method.getParameterTypes());
		ri.setMethodName(method.getName());
		ri.setArguments(arguments);
		
		final PipedOutputStream pout = new PipedOutputStream();
		final PipedInputStream pin = new PipedInputStream(pout);
		httpEntitiy.setContent(pin);
		final ObjectOutputStream out = new ObjectOutputStream(pout);
		
		final Thread thread = new Thread() {
			@Override
			public void run() {
				try {
					out.writeObject(ri);
					out.close();
				} catch (Exception e) {
					// TODO
					e.printStackTrace();
				}
			}
		};
		thread.start();
		
		return httpEntitiy;
	}
	
	private static <T> T extract(RemoteInvocationResult ir, Class<? extends T> cls) throws NuclosFatalIdeException, IOException, ClassNotFoundException {
		Throwable ex = ir.getException();
		if (ex != null) {
			// Unwrap remote exception
			if (ex instanceof InvocationTargetException) {
				ex = ex.getCause();
			}
			throw new NuclosFatalIdeException(ex);
		}
		final T value = (T) ir.getValue();
		return value;
	}
	
	private static String getMessageFromThrowable(Throwable ex) {
		if (ex != null) {
			if (ex.getMessage() != null) {
				return ex.getMessage();
			}
			if (ex.getCause() != null) {
				return getMessageFromThrowable(ex.getCause());
			}
		}
		return null;
	}
	
}
