//Copyright (C) 2014  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.ide.singleton;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.eclipse.core.resources.IProject;
import org.nuclos.api.ide.valueobject.ISourceItem;

/**
 * Spring component to track information on java code compiled by Nuclos
 * (rules, business objects, business support objects). This is the 
 * central place that 'knows' if java code has changed.
 * <p>
 * Information is retrieved by the fully qualified (java) class name.
 * </p>
 * @see ISourceItem
 * @see org.nuclos.api.ide.valueobject.SourceType
 * @author Thomas Pasch
 */
public class SourceItemCache {
	
	private static final ConcurrentHashMap<IProject,SourceItemCache> INSTANCES = new ConcurrentHashMap<IProject, SourceItemCache>();
	
	//
	
	private final Map<String, ISourceItem> qn2Item = new ConcurrentHashMap<String, ISourceItem>();
	
	private final Map<String, String> dirtySet = new ConcurrentHashMap<String, String>();
	
	/**
	 * Because of the lifecycle of an eclipse plugin, we don't know if we are dirty before a 
	 * server download has been triggered. In this case we must assume the all is dirty.
	 * 
	 * @author Thomas Pasch
	 */
	private boolean allIsDirty = true;
	
	private IProject project;
	
	private SourceItemCache(IProject project) {
		if (project == null) {
			throw new NullPointerException();
		}
		this.project = project;
	}
	
	public static SourceItemCache getInstance(IProject project) {
		SourceItemCache result = INSTANCES.get(project);
		if (result == null) {
			result = new SourceItemCache(project);
			INSTANCES.put(project, result);
		}
		return result;
	}
	
	public void hasUpOrDownloaded() {
	 	allIsDirty = false;
	}
	
	public void addOrUpdate(ISourceItem si) {
		final String key = si.getQualifiedName();
		qn2Item.put(key, si);
	}
	
	public void remove(String qualifiedName) {
		qn2Item.remove(qualifiedName);
		dirtySet.remove(qualifiedName);
	}
	
	public void dirty(String qualifiedName) {
		dirtySet.put(qualifiedName, qualifiedName);
	}
	
	public void notDirty(String qualifiedName) {
		dirtySet.remove(qualifiedName);
	}
	
	public boolean isDirty(String qualifiedName) {
		// return true;
		if (allIsDirty) return true;
		return dirtySet.containsKey(qualifiedName);
	}
	
	public void receivedDigest(String qualifiedName, String digest) {
		final ISourceItem si = qn2Item.get(qualifiedName);
		if (si != null) {
			if (si.getHashValue() != null) {
				if (!si.getHashValue().equals(digest)) {
					// This is an error only after the first sync - but why?
					if (!allIsDirty) {
						Errors.getInstance(project).addError(qualifiedName 
								+ " hash differs: server: " + si.getHashValue() + " ide: " + digest);
					}
					// use hash calculated here - it's the most recent one (tp)
					si.setHashValue(digest);
				}
			} else {
				si.setHashValue(digest);
			}
			// si.setHashValue(digest);
		}
	}
	
	public ISourceItem getSourceItem(String qualifiedName) {
		return qn2Item.get(qualifiedName);
	}

}
