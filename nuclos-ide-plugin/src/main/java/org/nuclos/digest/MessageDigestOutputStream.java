//Copyright (C) 2014  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.digest;

import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.eclipse.equinox.security.storage.EncodingUtils;
import org.nuclos.ide.NuclosFatalIdeException;

/**
 * Calculate a crypto hash while writing an OutputStream.
 *  
 * @author Thomas Pasch
 */
public class MessageDigestOutputStream extends FilterOutputStream {

	private final MessageDigest digest;

	public MessageDigestOutputStream(OutputStream in, String algo) throws NuclosFatalIdeException {
		super(in);
		try {
			digest = MessageDigest.getInstance(algo);
		} catch (NoSuchAlgorithmException e) {
			throw new NuclosFatalIdeException(e);
		}
	}

	@Override
	public void write(int b) throws IOException {
		super.write(b);
		digest.update((byte) b);
	}

	@Override
	public void write(byte b[]) throws IOException {
		super.write(b);
		digest.update(b, 0, b.length);
	}

	@Override
	public void write(byte b[], int off, int len) throws IOException {
		super.write(b, off, len);
		digest.update(b, off, len);
	}

	public byte[] digest() {
		return digest.digest();
	}

	public String digestAsHex() {
		return new String(org.nuclos.digest.MessageDigest.bytesToHex(digest.digest()));
	}
	
	public String digestAsBase64() {
		return EncodingUtils.encodeBase64(digest.digest());
	}

}
